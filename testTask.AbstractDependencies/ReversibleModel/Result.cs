﻿namespace testTask.AbstractDependencies.ReversibleModel
{
    public class Result
    {
        public string Code { get; set; }
        public bool Success { get; private set; }
        public string Message { get; private set; }
        public bool Failure => !Success;
        public Exception Exception { get; private set; }

        protected Result(Exception exception)
        {
            Code = "exception";
            Exception = exception;
            Success = false;
            Message = exception.ToString();
        }

        protected Result(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        protected Result(bool success, string code, string message)
        {
            Code = code;
            Success = success;
            Message = message;
        }

        public static Result Fail(string message)
        {
            return new Result(false, message);
        }

        public static Result Fail(Exception exception)
        {
            return new Result(exception);
        }

        public static Result Fail(string code, string message)
        {
            return new Result(false, code, message);
        }

        public static Result<T> Fail<T>(string message)
        {
            return new Result<T>(string.Empty, false, message);
        }

        public static Result<T> Fail<T>(string code, Exception exception)
        {
            return new Result<T>(code, exception);
        }

        public static Result<T> Fail<T>(Exception exception)
        {
            return new Result<T>(exception);
        }

        public static Result<T> Fail<T>(T obj, string message)
        {
            return new Result<T>(obj, false, message);
        }

        public static Result<T> Fail<T>(string code, string message)
        {
            return new Result<T>(code, false, message);
        }

        public static Result Ok()
        {
            return new Result(true, string.Empty);
        }

        public static Result<T> Ok<T>(T value)
        {
            return new Result<T>(value, true, string.Empty);
        }

        public static Result<T> Ok<T>()
        {
            return new Result<T>(string.Empty);
        }

        public static Result Combine(params Result[] results)
        {
            foreach (var result in results)
                if (result.Failure)
                    return result;
            return Ok();
        }
    }

    public class Result<T> : Result
    {
        public T Value { get; private set; }

        protected internal Result(string message) : base(true, message)
        {
        }

        protected internal Result(T value, string message)
            : base(true, message)
        {
            Value = value;
        }

        protected internal Result(T value, bool success, string message)
            : base(success, message)
        {
            Value = value;
        }

        protected internal Result(string code, bool success, string message)
            : base(success, message)
        {
            if (!string.IsNullOrEmpty(code))
                Code = code;
        }

        protected internal Result(string code, Exception exception) : base(exception)
        {
            if (!string.IsNullOrEmpty(code))
                Code = code;
        }

        protected internal Result(Exception exception) : base(exception)
        {
        }
    }
}
