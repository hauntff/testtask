﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using testTask.Domain.DbModels;

namespace testTask.Domain.DTO
{
    public class CategoryModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<ProductDetails>? ProductDetails { get; set; }
    }
}
