﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTask.Domain.DbModels;

namespace testTask.Domain.DTO
{
    public class ProductModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ProductData ProductData { get; set; }
        [ForeignKey("ProductData")] public int ProductDataId { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }
    }
}
