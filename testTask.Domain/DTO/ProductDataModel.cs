﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testTask.Domain.DTO
{
    public class ProductDataModel 
    {
        public int DetailId { get; set; }
        public string Value { get; set; }
    }
}
