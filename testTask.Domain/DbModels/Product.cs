﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTask.Domain.DTO;

namespace testTask.Domain.DbModels
{
    public class Product : ProductModel
    {
        [Key]
        public int Id { get; set; }
    }
}
