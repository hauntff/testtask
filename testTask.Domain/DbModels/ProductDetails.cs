﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using testTask.AbstractDependencies.Interfaces;
using testTask.Domain.DTO;

namespace testTask.Domain.DbModels
{
    public class ProductDetails : ProductDetailsModel
    {
        [Key]
        public int Id { get; set; }
    }
}
