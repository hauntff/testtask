﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace testTask.Application.Extensions
{
    public class ParametrExpressionVisitor : ExpressionVisitor
    {
        private readonly IReadOnlyCollection<ParameterExpression> _parameters1;
        private readonly IReadOnlyCollection<ParameterExpression> _parameters2;

        public ParametrExpressionVisitor(IReadOnlyCollection<ParameterExpression> parameters1, IReadOnlyCollection<ParameterExpression> parameters2)
        {
            _parameters1 = parameters1;
            _parameters2 = parameters2;

            if (parameters1 == null) throw new ArgumentNullException(nameof(parameters1));
            if (parameters2 == null) throw new ArgumentNullException(nameof(parameters2));
            if (parameters1.Count != parameters2.Count) throw new InvalidOperationException(
                "Parameter lengths must match");
            _parameters1 = parameters1;
            _parameters2 = parameters2;
        }
        protected override System.Linq.Expressions.Expression VisitParameter(ParameterExpression node)
        {
            for (int i = 0; i < _parameters1.Count; i++)
            {
                if (node == _parameters1.ElementAtOrDefault(i)) return _parameters2.ElementAt(i);
            }
            return node;
        }

        public static Expression<Func<T, bool>> AndExpression<T>(Expression<Func<T, bool>> exp1,
            Expression<Func<T, bool>> exp2)
        {
            if (exp1 is null && exp2 is null) return x => true;
            if (exp1 is null && exp2 is not null) return exp2;
            if (exp1 is not null && exp2 is null) return exp1;

            var newExp = new ParametrExpressionVisitor(exp2.Parameters, exp1.Parameters).VisitAndConvert(exp2.Body, "And");

            var expRes = Expression<Func<T, bool>>.And(exp1.Body, newExp);

            return System.Linq.Expressions.Expression.Lambda<Func<T, bool>>(expRes, exp1.Parameters);
        }

        public static Expression<Func<T, bool>> OrExpression<T>(Expression<Func<T, bool>> exp1,
            Expression<Func<T, bool>> exp2)
        {
            if (exp1 is null && exp2 is null) return x => true;
            if (exp1 is null && exp2 is not null) return exp2;
            if (exp1 is not null && exp2 is null) return exp1;

            var newExp = new ParametrExpressionVisitor(exp2.Parameters, exp1.Parameters).VisitAndConvert(exp2.Body, "And");

            var expRes = Expression<Func<T, bool>>.Or(exp1.Body, newExp);

            return System.Linq.Expressions.Expression.Lambda<Func<T, bool>>(expRes, exp1.Parameters);
        }
    }

    public static class ExpressionExtensions
    {
        public static Expression<Func<T, bool>> AndExpression<T>(this Expression<Func<T, bool>> exp1,
            Expression<Func<T, bool>> exp2)
        {
            return ParametrExpressionVisitor.AndExpression(exp1, exp2);
        }

        public static Expression<Func<T, bool>> OrExpression<T>(this Expression<Func<T, bool>> exp1,
            Expression<Func<T, bool>> exp2)
        {
            return ParametrExpressionVisitor.OrExpression(exp1, exp2);
        }
    }
}
