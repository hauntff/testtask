﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using testTask.AbstractDependencies.Interfaces;
using testTask.Infrastructure.DbContext;
using testTask.Infrastructure.Interfaces;
using testTask.Infrastructure.Repo;

namespace testTask.Application.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<ICategoryRepository, CategoriesRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IProductDataRepository, ProductDataRepository>();
            return services;
        }
        public static IServiceCollection AddContext(this IServiceCollection services)
        {
            services.AddDbContext<ProjectDbContext>(options =>
            {
                options.UseNpgsql("Host=localhost;Port=5432;Database=Test;Username=postgres;Password=gangoptimus");
                options.EnableSensitiveDataLogging();
            });
            return services;
        }
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            var assembly = Assembly.GetExecutingAssembly();
            return services.AddMediatR(assembly);
        }
    }
}
