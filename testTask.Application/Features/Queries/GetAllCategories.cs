﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Queries
{
    public class GetAllCategories : IRequest<Result<IEnumerable<Category>>>
    {
        public class GetAllCategoriesHandler : IRequestHandler<GetAllCategories, Result<IEnumerable<Category>>>
        {
            private readonly ICategoryRepository _categoryRepository;

            public GetAllCategoriesHandler(ICategoryRepository categoryRepository)
            {
                _categoryRepository = categoryRepository;
            }

            public async Task<Result<IEnumerable<Category>>> Handle(GetAllCategories allCategories,
                CancellationToken cancellationToken)
            {
                var result = await _categoryRepository.GetAll();
                return Result.Ok(result);
            }
        }
    }

}
