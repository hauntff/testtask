﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Queries
{
    public class GetProductByIdQuery : IRequest<Result<Product>>
    {
        public int Id { get; set; }
    }

    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, Result<Product>>
    {
        private readonly IProductRepository _repository;

        public GetProductByIdQueryHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<Result<Product>> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetProductById(request.Id, cancellationToken);
            return Result.Ok(result);
        }
    }
}
