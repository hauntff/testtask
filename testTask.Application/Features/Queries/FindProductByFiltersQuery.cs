﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Application.Extensions;
using testTask.Domain.DbModels;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Queries
{
    public class FindProductByFiltersQuery : IRequest<Result<IEnumerable<Product>>>
    {
        public int CategoryId { get; set; }
        public string[] QueryParams { get; set; }
    }

    public class FindProductByFiltersQueryHandler : IRequestHandler<FindProductByFiltersQuery, Result<IEnumerable<Product>>>
    {
        private readonly IProductRepository _repository;

        public FindProductByFiltersQueryHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<Result<IEnumerable<Product>>> Handle(FindProductByFiltersQuery query,
            CancellationToken cancellationToken)
        {
            var result = await _repository.Find(query.CategoryId, query.QueryParams, cancellationToken);

            return Result.Ok(result);
        }
    }
}
