﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore.Metadata;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Domain.DTO;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Commands
{
    public class CreateCategoryCommand : CategoryModel, IRequest<Result<Category>>
    {
        public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, Result<Category>>
        {
            private readonly ICategoryRepository _categoryRepository;

            public CreateCategoryCommandHandler(ICategoryRepository categoryRepository)
            {
                _categoryRepository = categoryRepository;
            }

            public async Task<Result<Category>> Handle(CreateCategoryCommand categoryCommand,
                CancellationToken cancellationToken)
            {
                Expression<Func<Category, bool>> expression = x =>
                    x.Name == categoryCommand.Name && x.Description == categoryCommand.Description;
                var existingCategory = await _categoryRepository.Get(expression);
                if (existingCategory.Any())
                {
                    return Result.Ok(existingCategory.FirstOrDefault());
                }
                var categoryModel = new Category()
                {
                    Description = categoryCommand.Description,
                    Name = categoryCommand.Description,
                    ProductDetails = categoryCommand.ProductDetails
                };
                await _categoryRepository.Insert(
                    categoryModel);
                return Result.Ok(categoryModel);
            }
        }

    }
}
