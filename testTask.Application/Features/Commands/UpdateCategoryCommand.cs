﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Domain.DTO;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Commands
{
    public class UpdateCategoryCommand : CategoryModel, IRequest<Result<Category>>
    {
        public int Id { get; set; }
    }

    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, Result<Category>>
    {
        private readonly ICategoryRepository _categoryRepository;

        public UpdateCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Result<Category>> Handle(UpdateCategoryCommand command, CancellationToken cancellationToken)
        {
            var existingCategory = await _categoryRepository.GetById(command.Id);
            if (existingCategory is null)
                return Result.Fail<Category>($"Category with id: {command.Id} was not found");

            existingCategory.Name = command.Name;
            existingCategory.Description = command.Description;
            existingCategory.ProductDetails = command.ProductDetails;

            try
            {
                await _categoryRepository.Update(existingCategory);
            }
            catch (Exception exception)
            {
                return Result.Fail<Category>($"Error: {exception.Message}");
            }

            return Result.Ok(existingCategory);
        }
    }

}
