﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Domain.DTO;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Commands
{
    public class CreateProductCommand : IRequest<Result<Product>>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProductDataId { get; set; }
        public string ImageUrl { get; set; }
        public decimal Price { get; set; }
        public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, Result<Product>>
        {
            private readonly IProductRepository _productRepository;

            public CreateProductCommandHandler(IProductRepository productRepository)
            {
                _productRepository = productRepository;
            }

            public async Task<Result<Product>> Handle(CreateProductCommand command, CancellationToken cancellationToken)
            {
                Expression<Func<Product, bool>> expression = x=>x.Name == command.Name && x.Description == command.Description;
                var existingProduct = await _productRepository.Get(expression);
                if (existingProduct.Any())
                    return Result.Ok(existingProduct.FirstOrDefault());

                var product = new Product
                {
                    Description = command.Description,
                    Name = command.Name,
                    ProductDataId = command.ProductDataId,
                    ImageUrl = command.ImageUrl,
                    Price = command.Price
                };
                try
                {
                    await _productRepository.Insert(product);
                }
                catch (Exception exception)
                {
                    return Result.Fail<Product>($"ERROR: {exception.StackTrace}");
                }
                return Result.Ok(product);
            }

        }

    }
}
