﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Domain.DTO;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Commands
{
    public class DeleteCategoryCommand : CategoryModel, IRequest<Result<Category>>
    {
        public int Id { get; set; }
    }

    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, Result<Category>>
    {
        private readonly ICategoryRepository _categoryRepository;

        public DeleteCategoryCommandHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<Result<Category>> Handle(DeleteCategoryCommand command, CancellationToken cancellationToken)
        {
            var existingCategory = await _categoryRepository.GetById(command.Id);
            if (existingCategory is null)
                return Result.Fail<Category>($"Category with id: {command.Id} was not found");

            try
            {
                await _categoryRepository.Delete(existingCategory);
            }
            catch (Exception exception)
            {
                return Result.Fail<Category>($"Error: {exception.Message}");
            }

            return Result.Ok(existingCategory);
        }
    }
}
