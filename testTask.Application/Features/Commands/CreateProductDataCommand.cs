﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Domain.DbModels;
using testTask.Domain.DTO;
using testTask.Infrastructure.Interfaces;

namespace testTask.Application.Features.Commands
{
    public class CreateProductDataCommand : ProductDataModel, IRequest<Result<ProductData>>
    {
        public class CreateProductDataCommandHandler : IRequestHandler<CreateProductDataCommand, Result<ProductData>>
        {
            private readonly IProductDataRepository _productDataRepository;

            public CreateProductDataCommandHandler(IProductDataRepository productDataRepository)
            {
                _productDataRepository = productDataRepository;
            }

            public async Task<Result<ProductData>> Handle(CreateProductDataCommand command,
                CancellationToken cancellationToken)
            {
                if (command.DetailId <= 0)
                {
                    return Result.Fail<ProductData>("Error, empty DetailId");
                }

                var newModel = new ProductData
                {
                    DetailId = command.DetailId,
                    Value = command.Value
                };

                await _productDataRepository.Insert(newModel);

                return Result.Ok(newModel);
            }
        }
    }
}
