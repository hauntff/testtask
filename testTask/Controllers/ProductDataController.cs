﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using testTask.Application.Features.Commands;
using ZstdSharp.Unsafe;

namespace testTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductDataController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductDataController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> Add(CreateProductDataCommand command, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(command, cancellationToken));
        }
    }
}
