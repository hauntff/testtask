﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using testTask.Application.Features.Commands;
using testTask.Application.Features.Queries;

namespace testTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoryController(IMediator mediator) => _mediator = mediator;

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _mediator.Send(new GetAllCategories()));
        }
        
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateCategoryCommand categoryCommand, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(categoryCommand, cancellationToken));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] UpdateCategoryCommand categoryCommand, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(categoryCommand, cancellationToken));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteCategoryCommand categoryCommand, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(categoryCommand, cancellationToken));
        }
    }
}
