﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using testTask.AbstractDependencies.ReversibleModel;
using testTask.Application.Features.Commands;
using testTask.Application.Features.Queries;

namespace testTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IMediator _mediator;
        public ProductController(IMediator mediator) => _mediator = mediator;


        [HttpPost("create")]
        public async Task<IActionResult> CreateProduct([FromBody] CreateProductCommand command, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(command, cancellationToken));
        }

        [HttpGet("findByFilters")]
        public async Task<IActionResult> Get([FromQuery] FindProductByFiltersQuery query, CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(query, cancellationToken));
        }

        [HttpGet("byId")]
        public async Task<IActionResult> GetById([FromQuery] GetProductByIdQuery query,
            CancellationToken cancellationToken)
        {
            return Ok(await _mediator.Send(query, cancellationToken));
        }
    }
}
