using System.Reflection;
using MediatR;
using testTask.Application.Extensions;
using testTask.Application.Features.Queries;
using testTask.Infrastructure.DbContext;
using testTask.Infrastructure.Interfaces;
using testTask.Infrastructure.Repo;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddContext();
builder.Services.AddApplication();
//builder.Services.AddMediatR(GetListOfEntryAssemblyWithReferences().ToArray());
//builder.Services.AddApplication();
builder.Services.AddRepositories();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "testTask.WebApi v1"));
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();



 static List<Assembly> GetListOfEntryAssemblyWithReferences()
{
    List<Assembly> listOfAssemblies = new List<Assembly>();
    var mainAsm = Assembly.GetEntryAssembly();
    listOfAssemblies.Add(mainAsm);

    foreach (var refAsmName in mainAsm.GetReferencedAssemblies())
    {
        listOfAssemblies.Add(Assembly.Load(refAsmName));
    }
    return listOfAssemblies;
}