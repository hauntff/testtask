﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTask.AbstractDependencies.Interfaces;
using testTask.AbstractDependencies.Repo;
using testTask.Domain.DbModels;
using testTask.Infrastructure.DbContext;
using testTask.Infrastructure.Interfaces;

namespace testTask.Infrastructure.Repo
{
    public class CategoriesRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoriesRepository(ProjectDbContext context) : base(context) { }

        public async Task<IEnumerable<Category>> GetAll()
        {
            var newContext = (ProjectDbContext)context;
            var result = newContext.Categories.AsEnumerable();

            return result;
        }
    }
}
