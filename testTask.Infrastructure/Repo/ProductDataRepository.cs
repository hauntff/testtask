﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTask.AbstractDependencies.Repo;
using testTask.Domain.DbModels;
using testTask.Infrastructure.DbContext;
using testTask.Infrastructure.Interfaces;

namespace testTask.Infrastructure.Repo
{
    public class ProductDataRepository : GenericRepository<ProductData>, IProductDataRepository
    {
        public ProductDataRepository(ProjectDbContext context) : base(context) { }
    }
}
