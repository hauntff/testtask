﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using testTask.AbstractDependencies.Repo;
using testTask.Domain.DbModels;
using testTask.Infrastructure.DbContext;
using testTask.Infrastructure.Interfaces;

namespace testTask.Infrastructure.Repo
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(ProjectDbContext context) : base(context) { }

        public async Task<Product> GetProductById(int id, CancellationToken cancellationToken)
        {
            var newContext = (ProjectDbContext)context;
            var query = newContext.Products
                .Include(x => x.ProductData)
                .Include(x => x.ProductData.DetailId)
                .Where(x => x.Id == id);

            return query.FirstOrDefault();

        }

        public async Task<IEnumerable<Product>> Find(int categoryId, string[] queryParams, CancellationToken cancellationToken)
        {
            var newContext = (ProjectDbContext)context;
            var query = newContext.Products
                .Include(x => x.ProductData)
                .Include(x => x.ProductData.DetailId)
                .Where(x=>true);

            if (categoryId > 0)
                query = query.Where(x => x.ProductDataId == categoryId);

            if (!query.Any())
                query = query.Where(x => queryParams.Contains(x.ProductData!.Value));

            return query;

        }
    }
}
