﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTask.AbstractDependencies.Interfaces;
using testTask.Domain.DbModels;

namespace testTask.Infrastructure.Interfaces
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<Product> GetProductById(int id, CancellationToken cancellationToken);
        Task<IEnumerable<Product>> Find(int categoryId, string[] queryParams, CancellationToken cancellationToken);
    }
}
